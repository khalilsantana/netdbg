use std::net::{IpAddr, SocketAddr, TcpStream, UdpSocket};

#[derive(Clone)]
pub enum PortStatus {
    OPEN,
    CLOSED,
    UNKNOWN,
}

#[derive(Clone, Debug)]
pub enum Proto {
    TCP,  // Transmission Control Protocol
    UDP,  // User Datagram Protocol
    ICMP, // Internet Control Message Protocol
    DNS,  // Domain Name System
    DOT,  // DNS over TLS
    HTTP, // Hyper Text Transfer Protocol
    TLS,  // Transport Layer Security
}

#[derive(Clone)]
pub struct Port {
    pub number: u16,
    pub proto: Proto,
    pub status: PortStatus,
}

pub struct Host<'a> {
    pub addr: IpAddr,
    pub hostname: &'a str,
    pub ports: Vec<Port>,
}

impl<'a> Host<'a> {
    pub fn new(addr: IpAddr, hostname: &'a str, ports: Vec<Port>) -> Self {
        Host {
            addr,
            hostname,
            ports,
        }
    }

    pub fn from(addr: &str, hostname: &'a str, ports: Vec<Port>) -> Self {
        Host {
            addr: addr.parse().unwrap(),
            hostname,
            ports,
        }
    }
}

impl Port {
    pub fn new(number: u16, proto: Proto) -> Self {
        Port {
            number,
            proto,
            status: PortStatus::UNKNOWN,
        }
    }
}

impl std::fmt::Display for Proto {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
