#![allow(dead_code, unused_imports, unused_variables)]
use std::io::prelude::*;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, TcpStream, UdpSocket};

use anyhow::{Context, Result};
use native_tls::TlsConnector;

mod model;
use model::*;

fn main() {
    let mut ports: Vec<Port> = Vec::new();
    ports.push(Port::new(80, Proto::TCP));
    ports.push(Port::new(443, Proto::TLS));
    let mut cloudflare_v4 = Host::from("1.1.1.1", "one.one.one.one", ports.clone());
    let mut cloudflare_v6 = Host::from("2606:4700:4700::1111", "one.one.one.one", ports.clone());

    try_connect(&mut cloudflare_v4);
    try_connect(&mut cloudflare_v6);

    print_host_stats(&cloudflare_v4);
    print_host_stats(&cloudflare_v6);
}

fn print_host_stats(host: &Host) {
    for port in &host.ports {
        let result = match port.status {
            PortStatus::OPEN => "success",
            _ => "failed",
        };
        println!(
            "Connecting to {HOST}:{PORT} using {PROTO} {RESULT}",
            HOST = host.addr,
            PROTO = port.proto,
            PORT = port.number,
            RESULT = result,
        )
    }
}

fn try_connect(host: &mut Host) {
    for port in host.ports.iter_mut() {
        let result = match port.proto {
            Proto::TCP => try_connect_tcp(&host.addr, &port.number),
            Proto::HTTP => try_connect_http(&host.addr, &port.number, &host.hostname),
            Proto::TLS => try_connect_tls(&host.addr, &port.number, &host.hostname),
            // TODO: Implement other protos
            _ => unimplemented!("protocol"),
        };
        if result.is_ok() {
            port.status = PortStatus::OPEN;
        }
    }
}

fn try_connect_tcp(host: &IpAddr, port: &u16) -> anyhow::Result<()> {
    let sock = SocketAddr::new(*host, *port);
    TcpStream::connect_timeout(&sock, std::time::Duration::from_secs(1))?;
    Ok(())
}

fn try_connect_http(host: &IpAddr, port: &u16, hostname: &str) -> anyhow::Result<()> {
    let sock = SocketAddr::new(*host, *port);
    let mut stream = TcpStream::connect_timeout(&sock, std::time::Duration::from_secs(1))?;
    // TODO: set user agent
    let msg = format!("GET / HTTP/1.1\r\nHost: {HOST}\r\n\r\n", HOST = hostname);
    stream.write(msg.as_bytes())?;
    stream.read(&mut [0; 10])?;
    Ok(())
}

fn try_connect_tls(host: &IpAddr, port: &u16, hostname: &str) -> anyhow::Result<()> {
    let sock = SocketAddr::new(*host, *port);
    let connector = TlsConnector::new()?;
    let stream = TcpStream::connect_timeout(&sock, std::time::Duration::from_secs(1))?;
    let mut stream = connector.connect(&hostname, stream)?;
    let msg = format!("GET / HTTP/1.1\r\nHost: {HOST}\r\n\r\n", HOST = hostname);
    stream.write_all(msg.as_bytes())?;
    stream.read_exact(&mut [0; 10])?;
    Ok(())
}
